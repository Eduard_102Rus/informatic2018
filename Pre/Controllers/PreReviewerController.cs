﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Pre.Models;

namespace Pre.Controllers
{
    public class PreReviewerController : Controller
    {
        private readonly Data.ApplicationDbContext _Db;
        private readonly UserManager<ApplicationUser> _userManager;
        public PreReviewerController(Data.ApplicationDbContext Db, UserManager<ApplicationUser> userManager)
        {
            this._Db = Db;
            this._userManager = userManager;
            //var dbcontextopts = new DbContextOptionsBuilder<Data.ApplicationDbContext>();
            //Db = new Data.ApplicationDbContext(dbcontextopts.Options);
        }


        public IActionResult Index()
        {
            if (_userManager.GetUserId(User) == null)
            {
                return RedirectToAction(nameof(AccountController.Login));
            }
            string iduser = _Db.Users.Where(x => x.Id == _userManager.GetUserId(User)).ToString();

            var result = _Db.Applications.Where(x => x.UserId == _userManager.GetUserId(User)).Select(x => x);
            return View(result);
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Models.App @app)
        {
            _Db.Applications.Add(new Models.App
            {
                Application_Name = app.Application_Name,
                Content = app.Content,
                Date_Making = DateTime.Now,
                Rating = 0,
                Verified = false,
                UserId = _userManager.GetUserId(User)

            });
            _Db.SaveChanges();

            return View();
        }




        public IActionResult Details(int id)
        {
            var result = _Db.Applications
                .Where(x => x.ID == id)
                .Select(x => x);

            return View(result);
        }

        public IActionResult Delete()
        {

            return View();

        }

    }
}