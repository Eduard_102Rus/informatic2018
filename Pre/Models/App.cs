﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pre.Models
{
    public class App
    {
        public int ID { get; set; }
        public string Application_Name { get; set; }
        public string Content { get; set; }
        public DateTime Date_Making { get; set; }
        public bool Verified { get; set; }
        public int Rating { get; set; }
        public string UserId { get; set; }
    }
}
